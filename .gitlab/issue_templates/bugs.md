### Description

Describe what happens and why you think it is a bug.

### Already check

* [ ] OTB correctly installed and initialized with "otbenv" script described in https://www.orfeo-toolbox.org/CookBook/Installation.html#recommended-installation-one-package-containing-all-the-modules
* [ ] Using QGis > 3.34
* [ ] Plugin installation conform to https://www.orfeo-toolbox.org/CookBook/QGISInterface.html 
* [ ] Python bindings recompiled **if using Linux with python > 3.8 or Windows with python != 3.10 ** (see https://www.orfeo-toolbox.org/CookBook/Installation.html#python-bindings) 
* [ ] from a clean QGis profile
* [ ] issue does not exists in QGis https://github.com/qgis/QGIS/issues?q=is%3Aopen+is%3Aissue+label%3ABug
* [ ] this is not an OTB application crash (see Journal part of plugin). If that the case, check if issues does not exists in [otb project](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/issues) and [submit you issue](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/issues/new)

### Steps to reproduce

Describe as precisely as possible how to reproduce the bug. Try to isolate a minimal number of steps. Also describe reproducibility (always, random ...).
If the test needs specific data to be reproduced, providing them will ease the bug resolution.

### Configuration information

* Operating system: 
* QGis Version (in Help > About) :
* OTB Plugin Version (Help > About, in active extension list) :
* OTB version or tag (see Processing tab, in first messages):
* How did you install OTB ? Downloaded from website, build from sources?
* Information related to build (binaries, superbuild, system libs ...)

### Logs (remove if useless)
Plugin logs that can helps (remove if useless):

**General log**

```bash
fill log that you can find in "General tab"
```

**Processing tab log**

```bash
fill log that you can find in "Processing tab"
```

**Extensions tab log**

```bash
fill log that you can find in "Extension tab"
```

***OTB execution logs**

```bash
fill log that you can find in during execution of OTB application in specific window
```

<!-- Do not remove following lines -->
/label ~bug
Mention @tristanlaurent or @thirom
