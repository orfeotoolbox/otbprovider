# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). **To keep compatibility with qgis-plugin-ci** do not use subsections in versions ([see qgis-plugin-ci issue 56](https://github.com/opengisch/qgis-plugin-ci/issues/56))
    
Changelogs before version 3.0.0 are based on QGis git blame of folder "python/plugins/otbprovider" at commit e9dd076

## [3.0.3] - 2024-04-30

- Fix ignored ParameterAsField parsing using parameterAsStrings (note plural)

## [3.0.2] - 2024-04-22

- Fix ignored user numeric parameter when equals to 0
- Replace deprecated parameterAsField by parameterAsString

## [3.0.1] - 2024-04-05

- Fix "No module named 'otbprovider.OtbChoiceWidget'" when opening some otbapplication using OtbChoiceWidget

## [3.0.0] - 2024-03-28

- Update metadata "about" field to remove deprecated QGis doc.

## [3.0.0-beta] - 2024-03-15

- Warn user when plugin is enabled but otb folder unset, makes sense now that otb is a third party plugin - by Tristan Laurent
- OTB provider now works with QGis 3.36 as a third party plugin - by Tristan Laurent

## [2.12.99] - 2023-11-24

- Add Test for otbprovider - by Alexander Bruy
- Use QGisUnitTest - by Matthias Kuhn
- Separate otb provider into a plugin - by Alexander Bruy
- Removed redundant "import qgis" - by Nyall Dawson
- Removed warning when plugin is enabled but otb folder unset - by Nyall Dawson
- Duplicate definitions for python imports - by Étienne Trimaille
- Drop unmainted saga provider from QGIS install - by Nyall Dawson
- Removed unused "Activate" plugin setting - by Alexander Bruy
- Fix OTB folder handling when it does not exists or not set correctly - by Alexander Bruy
